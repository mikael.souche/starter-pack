Mon 1er UML

```plantuml
participant User
participant PlantUML
participant "GIT UML PRECIA" as GitLab
User -> GitLab : Request 
User <- GitLab : Return
User -> PlantUML : Request 1
User <- PlantUML : Return 1
```

Mon 2ème UML

```plantuml
participant User
participant PlantUML
participant "GIT UML PRECIA" as GitLab
User -> GitLab : Request 
User <- GitLab : Return
User -> PlantUML : Request 1
User <- PlantUML : Return 1
```

Mon 1er diagram de classe

```plantuml
class A {
{static} int counter
+void {abstract} start(int timeout)
}
class B {
{static} int counter
+void {abstract} start(int timeout)
}
note right of A::counter
  This member is annotated
end note
note right of A::start
  This method is now explained in a UML note
end note

A -- B

```

Mon 2ème diagrame de classe

```plantuml
Class01 <|-- Class02
Class03 *-- Class04
Class05 o-- Class06
Class07 .. Class08
Class09 -- Class10
```

Mon 1er Use Case
```plantuml
skinparam actorStyle awesome
Use --> (Use the application)
Admin --> (Admin the application)
Admin --> (Admin the application2)
```

